## NOTE - MAIN REPOSITORY LOCATION HAS MOVED

The new location is https://gitlab.developers.cam.ac.uk/slcu/teamhj/publications/ham


## Introduction

Scripts and software connected to the publication

The interaction of transcription factors controls the spatial layout of plant aerial stem cell niches
Jérémy Gruel, Julia Deichmann, Benoit Landrein, Thomas Hitchcock & Henrik Jönsson (2018)
npj Systems Biology and Applicationsvolume 4, Article number: 36 (2018)

## Testing optimised parameters

```bash
python test_2d.py F R
```

where F is the file number from a successful optimisation (F should be in the OPT/ directory)
and R the radius of the meristem, R=30 is the wild type (R can vary between 10 and 49 without modifying the script)

the script will display and save (X.png) the components of the system and output a data file (X.data) containing variable values for every cell in wild type, clavata (R=30) and ham. Files are stored in the DATA directory.

The optimised parameter sets used in the manuscript are in the OPT/ directory.
The N_xxx files are the no pocket category
The A_xxx files are the pocket activator category
The R-_xxx files are the pocket repressor - category
The R+_xxx files are the pocket repressor + category

## Optimisation

```bash
python opt_2d.py X
```

where X is the job number.
If the optimisation is succesfull a parameter file "params_X" will be created in the OPT directory.

For collecting parameter sets, it is recommended to loop over the optimisation script while increasing the job number with every iteration.

Note1: not all runs of the optimisation script will yield a suitable parameter set
Note2: the stabilisation of the whole system requires to run the "test_2d.py" script. Some optimised parameter sets, once stabilised, may
have to be discarded from further analysis.

## Realistic meristem
The scripts are available at: https://gitlab.com/slcu/teamHJ/julia/HAM-Model

## Contact

henrik.jonsson@slcu.cam.ac.uk

